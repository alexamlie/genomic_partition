## plot_ggplot2_ANNOT_loci_genomic_partition.R
## alex amlie-wolf
## computes genomic partition of SPAR-called loci

plot_ggplot2_ANNOT_loci_genomic_partition <- function(fprefix='Aligned.out.filtered.hardClipped.sorted.max44.bam',wdir=".", partition_ref_path = "./partition_files/")  {
    datafile=paste(wdir, "/", fprefix, ".annot.final.csv", sep="")
    
    BASENAME="ANNOT_loci_genomic_partition"
    PLOTTITLE="Genomic partitioning of annotated loci\nacross sncRNA classes"
    XTITLE="Genomic_Element"
    YTITLE="Count"

    suppressPackageStartupMessages(library(plyr))
    suppressPackageStartupMessages(library(ggplot2))
    suppressPackageStartupMessages(library(RColorBrewer))
    suppressPackageStartupMessages(library(parallel))

    ## set up parallelization
    clust <- makeCluster(min(2, detectCores()))
    
    ## output image file
    pngfile= paste(wdir, "/", paste(BASENAME,".png",sep=""), sep="")
    pdffile= paste(wdir, "/", paste(BASENAME,".pdf",sep=""), sep="")
    
    ## generate bed file (use peak strand)
    locus_data <- read.table(datafile, header=T, sep="\t", quote="", as.is=T,
                             comment.char="", strip.white=T)
    ## define an index for the extra columns we want to paste
    locus_col_idx <- which(!(colnames(locus_data) %in% c("X.chr", "chrStart", "chrEnd", "strand", "annotRNAclass")))

    ## write the full bed file
    write.table(data.frame(locus_data$X.chr, locus_data$chrStart, locus_data$chrEnd,
                           gsub("\\s*", "",
                                apply(cbind(locus_data$annotRNAclass, locus_data[,locus_col_idx]), 1, paste, collapse=",")),
                           "0", locus_data$strand),
                paste0(wdir, "/", BASENAME, ".bed"), quote=F, sep="\t",
                row.names=F, col.names=F)
    rm(locus_data)
    
    ## sort this bed file
    system(paste("sort -k1,1V -k2,2n", paste0(wdir, "/", BASENAME, ".bed"),
                 ">", paste0(wdir, "/", BASENAME, "_sorted.bed"))) 
    
    ## split by strand, write to files
    split_res <- parLapply(clust,
                           c(paste("awk '{if ($6==\"+\") print $0}'",
                                   paste0(wdir, "/", BASENAME, "_sorted.bed"),
                                   ">", paste0(wdir, "/", BASENAME, "_pos.bed")),
                             paste("awk '{if ($6==\"-\") print $0}'",
                                   paste0(wdir, "/", BASENAME, "_sorted.bed"),
                                   ">", paste0(wdir, "/", BASENAME, "_neg.bed"))),
                           function(x) {system(x)})
                             
    ## remove other files
    system(paste("rm", paste0(wdir, "/", BASENAME, "_sorted.bed"),
                 paste0(wdir, "/", BASENAME, ".bed")))
    
    ## call the overlap computation script on each strand
    ## assumes that the python script is in this folder
    pos_partition_call <- paste("python ", paste0(partition_ref_path, "/compute_genomic_element_overlap.py"),
                 paste0(partition_ref_path, "/parsed_pos_5utr_exons.merged.bed"), 
                 paste0(partition_ref_path, "/pos_n5e_5utr_introns.bed"), 
                 paste0(partition_ref_path, "/pos_n5e5i_3utr_exons.bed"), 
                 paste0(partition_ref_path, "/pos_n5e5i3e_3utr_introns.bed"), 
                 paste0(partition_ref_path, "/pos_n5e5i3e3i_promoters.bed"), 
                 paste0(partition_ref_path, "/pos_n5e5i3e3ip_exons.bed"), 
                 paste0(partition_ref_path, "/pos_n5e5i3e3ipe_introns.bed"), 
                 paste0(partition_ref_path, "/pos_n5e5i3e3ipei_repeats.bed"),
                 paste0(wdir, "/", BASENAME, "_pos.bed"),
                 paste0(wdir, "/", BASENAME, "_pos_partition.txt"))
    
    neg_partition_call <- paste("python", paste0(partition_ref_path, "/compute_genomic_element_overlap.py"),
                 paste0(partition_ref_path, "/parsed_neg_5utr_exons.merged.bed"), 
                 paste0(partition_ref_path, "/neg_n5e_5utr_introns.bed"), 
                 paste0(partition_ref_path, "/neg_n5e5i_3utr_exons.bed"), 
                 paste0(partition_ref_path, "/neg_n5e5i3e_3utr_introns.bed"), 
                 paste0(partition_ref_path, "/neg_n5e5i3e3i_promoters.bed"), 
                 paste0(partition_ref_path, "/neg_n5e5i3e3ip_exons.bed"), 
                 paste0(partition_ref_path, "/neg_n5e5i3e3ipe_introns.bed"), 
                 paste0(partition_ref_path, "/neg_n5e5i3e3ipei_repeats.bed"),
                 paste0(wdir, "/", BASENAME, "_neg.bed"),
                 paste0(wdir, "/", BASENAME, "_neg_partition.txt"))

    ## store to variable so it doesn't print out 
    partition_res <- parLapply(clust, c(pos_partition_call, neg_partition_call),
                               function(x) { system(x) } )
    
    ## assign to partitions
    pos_partition_data <- read.table(paste0(wdir, "/", BASENAME, "_pos_partition.txt"), header=T, sep="\t", quote="", as.is=T)
    neg_partition_data <- read.table(paste0(wdir, "/", BASENAME, "_neg_partition.txt"), header=T, sep="\t", quote="", as.is=T)

    ## merge the tables
    full_partition_data <- rbind(pos_partition_data, neg_partition_data)
    
    ## add a class column:
    full_partition_data$class <- unlist(lapply(strsplit(full_partition_data$name, ","), '[', 1))   
    
    ## first do assignment over all loci
    full_partition_data$partition <- ifelse(full_partition_data$fp_utr_exon_bp > 0, "5' UTR Exon",
                                     ifelse(full_partition_data$fp_utr_intron_bp > 0, "5' UTR Intron",
                                     ifelse(full_partition_data$tp_utr_exon_bp > 0, "3' UTR Exon",
                                     ifelse(full_partition_data$tp_utr_intron_bp > 0, "3' UTR Intron",
                                     ifelse(full_partition_data$promoter_bp > 0, "Promoter",
                                     ifelse(full_partition_data$exon_bp > 0, "mRNA Exon",
                                     ifelse(full_partition_data$intron_bp > 0, "mRNA Intron",
                                     ifelse(full_partition_data$repeat_bp > 0, "Repeat",
                                     ## here we check if it's a canonical chromosome
                                     ifelse(grepl("chr[0-9XY]{1,2}$", full_partition_data$chr), "Intergenic",
                                     ## if not, we just give it the chromosome name
                                            full_partition_data$chr)))))))))

    ## write this table out
    write.table(full_partition_data,
                paste0(wdir, "/", BASENAME, "_allclass_partition_assignment.txt"), quote=F, sep="\t", row.names=F, col.names=T)

    ## now get the proportions and counts
    ## define the desired order of (known) partition classes
    partition_order <- c("5' UTR Exon", "5' UTR Intron", "3' UTR Exon", "3' UTR Intron",
                         "Promoter", "mRNA Exon", "mRNA Intron", "Repeat", "Intergenic")
    
    full_prop_tab <- as.data.frame(table(full_partition_data$partition), stringsAsFactors = F)
    full_prop_tab$prop <- full_prop_tab$Freq / sum(full_prop_tab$Freq)

    ## re-order    
    full_prop_tab <- full_prop_tab[match(c(partition_order, as.character(full_prop_tab$Var1[!(full_prop_tab$Var1 %in% partition_order)])), full_prop_tab$Var1),]

    colnames(full_prop_tab) <- c("Genomic_Element", "Count", "Proportion")
    
    write.table(full_prop_tab, paste0(wdir, "/", BASENAME, "_allclass_partition_props.txt"), quote=F, sep="\t", row.names=F, col.names=T)
    
    ## then look at assignment per sncRNA class
    class_prop_tab <- ddply(full_partition_data, .(class), function(x) {
        count_df <- as.data.frame(table(x$partition), stringsAsFactors = F)
        count_df$prop <- count_df$Freq / sum(count_df$Freq)
        ## add entries for those classes that we didn't observe
        if (sum(!(partition_order %in% count_df$Var1)) > 0) {
            count_df <- rbind(count_df,
                              data.frame(Var1=partition_order[!(partition_order %in% count_df$Var1)], Freq=0, prop=0))
        }
        
        ## re-order    
        count_df <- count_df[match(c(partition_order, as.character(count_df$Var1[!(count_df$Var1 %in% partition_order)])), count_df$Var1),]

        colnames(count_df) <- c("Genomic_Element", "Count", "Proportion")
        return(count_df)
    })

    write.table(class_prop_tab, paste0(wdir, "/", BASENAME, "_partition_props_byclass.txt"), quote=F, sep="\t", row.names=F, col.names=T)
    
    ## visualize proportions
    ## first index the elements that aren't in 'standard' partitions
    non_std_element_idx <- !(unique(full_prop_tab[,"Genomic_Element"]) %in% partition_order)

    ## collapse into 'other'
    output_prop_df <- rbind(full_prop_tab[full_prop_tab[,"Genomic_Element"] %in% partition_order,],
                            data.frame(Genomic_Element="Other",
                                       Count=sum(full_prop_tab[non_std_element_idx,"Count"]),
                                       Proportion=sum(full_prop_tab[non_std_element_idx,"Proportion"])))
    output_prop_df$position <- cumsum(output_prop_df$Count) - 0.5*output_prop_df$Count

    getPalette = colorRampPalette(brewer.pal(n=10, "Paired"))
    options(scipen=10000)
    
    chart = ggplot(output_prop_df, aes(x="", y=output_prop_df[,YTITLE], fill=output_prop_df[,XTITLE]))+
            geom_bar(width = 1, stat = "identity")+ 
            coord_polar("y", start=0)+
            scale_fill_manual(values=c(setNames(brewer.pal(n=9, name="Paired"), partition_order), "Other"="#4D4D4D"),name=XTITLE)+
            ggtitle(PLOTTITLE) + xlab("")+ylab("")+
            geom_text(aes(label = sprintf("%1.1f%%", round(output_prop_df$Proportion,3)*100), y = output_prop_df$position))+
            theme(axis.text = element_text(size = 12),axis.title = element_text(size =14),plot.title = element_text(size = 16))
        
    ggsave(pngfile,chart,dpi=600,type="cairo-png")
    
    ggsave(pdffile,chart,dpi=600)
    
    ## plot each class partitions as stacked barplots
    ## first collapse the non-standard ones
    classwise_output_df <- ddply(class_prop_tab, .(class), function(x) {
        this_class <- unique(x$class)
        non_std_element_idx <- !(unique(x$Genomic_Element) %in% partition_order)
        return(rbind(x[!non_std_element_idx,],
                     data.frame(class=this_class, Genomic_Element="Other",
                                Count=sum(x$Count[non_std_element_idx]),
                                Proportion=sum(x$Proportion[non_std_element_idx]))))
    })

    
    
    class_barplots <- ggplot(classwise_output_df, aes(x=class, y=Proportion, fill=Genomic_Element)) +
        scale_fill_manual(values=c(setNames(brewer.pal(n=9, name="Paired"), partition_order), "Other"="#4D4D4D"), name=XTITLE) +
        xlab("sncRNA Class") + ylab("Genomic Partition Proportions") +
        theme_bw() + geom_bar(stat="identity", position="stack") +
        theme(legend.position="bottom", axis.text.x=element_text(angle=45, hjust=1)) +
        guides(fill=guide_legend(nrow=3, byrow=TRUE)) + 
        ggtitle(paste("Genomic Proportions in", BASENAME))
    
    ggsave(paste0(wdir, "/", BASENAME, "_partition_byclass.png"), class_barplots, dpi=600, type="cairo-png")
    ggsave(paste0(wdir, "/", BASENAME, "_partition_byclass.pdf"), class_barplots, dpi=600)
    
}

# plot_ggplot2_ANNOT_loci_genomic_partition(fprefix=fprefix,wdir=wdir, partition_ref_path = partition_ref_path)
