plot_ggplot2_ANNOT_loci_perclass<- function(fprefix='Aligned.out.filtered.hardClipped.sorted.max44.bam',wdir=".") {

# Plots for Module_3
# Analysis and visualization of SPAR output 

# Module_3_Figure_3 (Figure 3.3) 
# description of figure: No (and proportion) of annotated loci per sncRNA class 
# input: 
# Aligned.out.filtered.hardClipped.sorted.max44.bam.annot.final
# output: ANNOT_loci_perclass.png / ANNOT_loci_perclass.pdf
# comment: No (and proportion) of annotated loci per sncRNA class 
print(fprefix)
print(wdir)

datafile=paste(wdir, "/", fprefix, ".annot.final.csv", sep="")
BASENAME="ANNOT_loci_perclass"
PLOTTITLE="No (and proportion) of annotated loci per sncRNA class "
XTITLE="sncRNA_class"
YTITLE="Count"

suppressPackageStartupMessages(library(reshape2))
suppressPackageStartupMessages(library(ggplot2))
suppressPackageStartupMessages(library(plyr))
suppressPackageStartupMessages(library(RColorBrewer))

#args<-commandArgs(TRUE)
#datafile=args[1] # input file
#wdir=args[2] # output / working directory
#if (length(args)<1) { stop("ERROR: No input! USAGE: script inputfile <output-dir>")}
#if (length(args)<2) { wdir="." } 
#use current dir if no 
#working dir has been specified

# output image file
pngfile= paste(wdir, "/", paste(BASENAME,".png",sep=""), sep="")
pdffile= paste(wdir, "/", paste(BASENAME,".pdf",sep=""), sep="")

D = read.table(datafile,sep='\t',header=T,comment.ch="")
D$Loci_length= D$chrEnd-D$chrStart
final = D[,c("Loci_length","annotRNAclass","expressionValue")]
colnames(final) = c("Loci_length",XTITLE,"Expression_log10_RAW")
final_Count = final[which(final$Loci_length<=100),]

# prepare to include the 'position' information for text 
z = table(final_Count[,XTITLE])
zmelt = melt(z,ID=1)
colnames(zmelt)=c(XTITLE,YTITLE)
combine_f = ddply(zmelt, .(), transform, weight=Count/sum(Count))[,-1]
m2 = ddply(zmelt, .(), transform, position = cumsum(Count) - 0.5*Count) 
combine_f =  cbind(combine_f,m2[,4])
colnames(combine_f)[4] = c("position")

# for plotting 
colourCount = dim(table(final_Count[,XTITLE]))
getPalette = colorRampPalette(brewer.pal(colourCount, "Set3"))
options(scipen=10000)

chart = ggplot(combine_f, aes(x="", y=combine_f[,YTITLE], fill=combine_f[,XTITLE]))+
geom_bar(width = 1, stat = "identity")+ 
coord_polar("y", start=0)+
scale_fill_manual(values = getPalette(colourCount),name=XTITLE)+
ggtitle(PLOTTITLE) + xlab("")+ylab("")+
geom_text(aes(label = paste(as.character(combine_f[,XTITLE]),sprintf("%1.1f%%", round(combine_f$weight,3)*100),sep="-"), y = combine_f$position))+
theme(axis.text = element_text(size = 12),axis.title = element_text(size =14),plot.title = element_text(size = 16))

ggsave(pdffile,chart,dpi=600)
ggsave(pngfile,chart,dpi=600,type="cairo-png")

}

plot_ggplot2_ANNOT_loci_perclass(fprefix=fprefix,wdir=wdir)









