## 08_29_16_partition_with_gene_names_generation.sh
## alex amlie-wolf
## recording the steps i'm taking on scisub to create partition files that keep gene names

INPUT_DIR=~/data/refgenomes/hg19/protein_coding_utrs/input_files/
## make one directory to store all these analyses
OUTDIR=~/data/refgenomes/hg19/protein_coding_utrs/annotations_with_gene_names/
## make directories to store the bed files, and also the parsed (and sorted) versions
mkdir -p ${OUTDIR}/bed_files/ ${OUTDIR}/parsed_files/

echo "Parsing gffs"
for F in ${INPUT_DIR}/hsa19.splitByType.*; do
    echo $F
    TYPE=`echo $F | cut -d. -f3`
    awk -F $'\t' '{ printf "%s\t%d\t%d\t%s\t0\t%s\n", $1, ($4-1), $5, $10, $7 }' $F > ${OUTDIR}/bed_files/${TYPE}.bed
done

cd ${OUTDIR}/bed_files/

## copy over the repeat and promoter stuff
OLD_BED_DIR=~/data/refgenomes/hg19/protein_coding_utrs/bed_files/
cp -t . ${OLD_BED_DIR}/mRNA_promoters.bed ${OLD_BED_DIR}/repeats.bed 

echo "Parsing bed files for canonical chromosomes"
for F in *; do
    echo $F    
    ~/code/bed_statistics/parse_bedfile_chrs.sh $F ${OUTDIR}/parsed_files/parsed_${F}
done

mkdir -p ${OUTDIR}/strand_split_files/
cd ${OUTDIR}/parsed_files/

echo "Splitting files by strand"
for F in *; do
    echo $F
    CLASS=`echo $F | cut -d. -f1`
    awk '{if ($6=="+") print $0}' $F > ${OUTDIR}/strand_split_files/${CLASS}.pos.bed
    awk '{if ($6=="-") print $0}' $F > ${OUTDIR}/strand_split_files/${CLASS}.neg.bed
done

## now merge these files together
cd ${OUTDIR}/strand_split_files/
mkdir -p ${OUTDIR}/merged_files/

## finally, generate the exclusive elements (need bedtools)
module load bedtools2

echo "Merging files"
## merge all of them
for F in *; do
    echo $F
    bedtools merge -i ${F} -c 4 -o distinct -delim ";" > ${OUTDIR}/merged_files/${F%.bed}.merged.bed
done

cd ${OUTDIR}/merged_files/
echo "Sorting merged files"
## sort the merged files
for F in *.bed; do
    echo $F
    sort -k1,1V -k2,2n $F > $F.sorted
    mv $F.sorted $F
done

## run the full exclusive element generation script
echo "Generating exclusive elements"
~/code/bed_statistics/generate_full_utr_exclusive_elements.sh \
    ~/data/refgenomes/hg19/hg19.chrom.sizes.sorted ${OUTDIR}/merged_files/ ${OUTDIR}

## finally, merge each of these files together
for STRAND in neg pos; do 
    cd ${OUTDIR}/${STRAND}_files/final_files/
    ## create the full merged file
    echo "Creating full merged files on ${STRAND} strand"
    >${STRAND}_all_annotations.bed

    awk -F$'\t' 'BEGIN{OFS=FS} {$4="5utr_exon;"$4; print}' \
	parsed_${STRAND}_5utr_exons.merged.bed >> ${STRAND}_all_annotations.bed
    awk -F$'\t' 'BEGIN{OFS=FS} {$4="5utr_intron;"$4; print}' \
	${STRAND}_n5e_5utr_introns.bed >> ${STRAND}_all_annotations.bed
    awk -F$'\t' 'BEGIN{OFS=FS} {$4="3utr_exon;"$4; print}' \
	${STRAND}_n5e5i_3utr_exons.bed >> ${STRAND}_all_annotations.bed
    awk -F$'\t' 'BEGIN{OFS=FS} {$4="3utr_intron;"$4; print}' \
	${STRAND}_n5e5i3e_3utr_introns.bed >> ${STRAND}_all_annotations.bed
    awk -F$'\t' 'BEGIN{OFS=FS} {$4="promoter;"$4; print}' \
	${STRAND}_n5e5i3e3i_promoters.bed >> ${STRAND}_all_annotations.bed
    awk -F$'\t' 'BEGIN{OFS=FS} {$4="exon;"$4; print}' \
	${STRAND}_n5e5i3e3ip_exons.bed >> ${STRAND}_all_annotations.bed
    awk -F$'\t' 'BEGIN{OFS=FS} {$4="intron;"$4; print}' \
	${STRAND}_n5e5i3e3ipe_introns.bed >> ${STRAND}_all_annotations.bed
    awk -F$'\t' 'BEGIN{OFS=FS} {$4="repeat;"$4; print}' \
	${STRAND}_n5e5i3e3ipei_repeats.bed >> ${STRAND}_all_annotations.bed

    sort -k1,1V -k2,2n ${STRAND}_all_annotations.bed > ${STRAND}_all_annotations.sorted.bed
    mv ${STRAND}_all_annotations.sorted.bed ${STRAND}_all_annotations.bed
done
