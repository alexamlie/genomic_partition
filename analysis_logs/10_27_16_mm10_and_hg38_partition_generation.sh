#!/bin/sh

## 10_27_16_mm10_and_hg38_partition_generation.sh
## alex amlie-wolf
## the steps to create reference files for SPAR of hg38 and mm10 reference genomes
## this one is run partially on tesla and partially on scisub

## ---------------------------------------------------------
for REF in mm10 hg38; do
    echo "Parsing reference ${REF}"

    # ## parameters to run on Tesla (create the split bed files)
    # GFF_FILE=/mnt/data/users/pkuksa/datasets/${REF}/${REF}.DASHR.gff
    # OUTDIR=/home/alexaml/refgenomes/${REF}/

    # awk -F $'\t' -v OUT=${OUTDIR} '{OUTF=OUT"/"$3".bed"; printf "%s\t%d\t%d\t%s\t0\t%s\n", $1, ($4-1), $5, $9, $7 > OUTF}' ${GFF_FILE}
    
    # ## now create the promoter file
    # awk -F $'\t' -v OUT=${OUTDIR} \
    # 	'{if ($3=="mRNA") {
    # 	      if ($7=="+") {
    # 		if ($4-1001 >= 0) { printf "%s\t%d\t%d\t%s\t0\t+\n", $1, ($4-1001), $4, $9 }
    # 		else { printf "%s\t0\t%d\t%s\t0\t+\n", $1, $4, $9} }
    # 	      else { printf "%s\t%d\t%d\t%s\t0\t%s\n", $1, ($5-1), ($5+1000), $9, $7 } }}' \
    # 		  ${GFF_FILE} > ${OUTDIR}/mRNA_promoters.bed

    ## parameters for scisub (after everything is copied over)
    OUTDIR=/home/alexaml/data/refgenomes/${REF}/SPAR_partitions/
    
    mkdir -p ${OUTDIR}/bed_files/ ${OUTDIR}/parsed_files/

    cd ${OUTDIR}/bed_files/
    
    echo "Parsing bed files for canonical chromosomes"
    for F in *; do
    	echo $F    
    	~/code/bed_statistics/parse_bedfile_chrs.sh $F ${OUTDIR}/parsed_files/parsed_${F}
    done

    mkdir -p ${OUTDIR}/strand_split_files/
    cd ${OUTDIR}/parsed_files/

    echo "Splitting files by strand"
    for F in *; do
    	echo $F
    	CLASS=`echo $F | cut -d. -f1`
    	awk '{if ($6=="+") print $0}' $F > ${OUTDIR}/strand_split_files/${CLASS}.pos.bed
    	awk '{if ($6=="-") print $0}' $F > ${OUTDIR}/strand_split_files/${CLASS}.neg.bed
    done

    ## now merge these files together
    cd ${OUTDIR}/strand_split_files/
    mkdir -p ${OUTDIR}/merged_files/

    module load bedtools2
    
    echo "Merging files"
    ## merge all of them
    for F in *; do
    	echo $F
    	bedtools merge -i ${F} -c 4 -o distinct -delim ";" > ${OUTDIR}/merged_files/${F%.bed}.merged.bed
    done

    cd ${OUTDIR}/merged_files/
    echo "Sorting merged files"
    ## sort the merged files
    for F in *.bed; do
    	echo $F
    	sort -k1,1V -k2,2n $F > $F.sorted
    	mv $F.sorted $F
    done

    ## run the full exclusive element generation script
    echo "Generating exclusive elements"
    ~/code/bed_statistics/generate_full_utr_exclusive_elements.sh \
    	~/data/refgenomes/hg19/hg19.chrom.sizes.sorted ${OUTDIR}/merged_files/ ${OUTDIR}

    ## merge each of these files together by strand
    for STRAND in neg pos; do 
    	cd ${OUTDIR}/${STRAND}_files/final_files/
    	## create the full merged file
    	echo "Creating merged files on ${STRAND} strand"
    	>${STRAND}_all_annotations.bed

    	awk -F$'\t' 'BEGIN{OFS=FS} {$4="5utr_exon;"$4; print}' \
    	    parsed_${STRAND}_5utr_exons.merged.bed >> ${STRAND}_all_annotations.bed
    	awk -F$'\t' 'BEGIN{OFS=FS} {$4="5utr_intron;"$4; print}' \
    	    ${STRAND}_n5e_5utr_introns.bed >> ${STRAND}_all_annotations.bed
    	awk -F$'\t' 'BEGIN{OFS=FS} {$4="3utr_exon;"$4; print}' \
    	    ${STRAND}_n5e5i_3utr_exons.bed >> ${STRAND}_all_annotations.bed
    	awk -F$'\t' 'BEGIN{OFS=FS} {$4="3utr_intron;"$4; print}' \
    	    ${STRAND}_n5e5i3e_3utr_introns.bed >> ${STRAND}_all_annotations.bed
    	awk -F$'\t' 'BEGIN{OFS=FS} {$4="promoter;"$4; print}' \
    	    ${STRAND}_n5e5i3e3i_promoters.bed >> ${STRAND}_all_annotations.bed
    	awk -F$'\t' 'BEGIN{OFS=FS} {$4="exon;"$4; print}' \
    	    ${STRAND}_n5e5i3e3ip_exons.bed >> ${STRAND}_all_annotations.bed
    	awk -F$'\t' 'BEGIN{OFS=FS} {$4="intron;"$4; print}' \
    	    ${STRAND}_n5e5i3e3ipe_introns.bed >> ${STRAND}_all_annotations.bed
    	awk -F$'\t' 'BEGIN{OFS=FS} {$4="repeat;"$4; print}' \
    	    ${STRAND}_n5e5i3e3ipei_repeats.bed >> ${STRAND}_all_annotations.bed

    	sort -k1,1V -k2,2n ${STRAND}_all_annotations.bed > ${STRAND}_all_annotations.sorted.bed
    	mv ${STRAND}_all_annotations.sorted.bed ${STRAND}_all_annotations.bed
    done

    ## finally, merge the strand files together
    echo "Merging strand files into final master file!"
    awk -F$'\t' 'BEGIN{OFS==FS} {printf $0"\t.\t-\n"}' ${OUTDIR}/neg_files/final_files/neg_all_annotations.bed > ${OUTDIR}/all_annotations_both_strand_with_names.bed
    awk -F$'\t' 'BEGIN{OFS==FS} {printf $0"\t.\t+\n"}' ${OUTDIR}/pos_files/final_files/pos_all_annotations.bed >> ${OUTDIR}/all_annotations_both_strand_with_names.bed

    sort -k1,1 -k2,2n ${OUTDIR}/all_annotations_both_strand_with_names.bed > ${OUTDIR}/all_annotations.sorted.bed
    mv ${OUTDIR}/all_annotations.sorted.bed ${OUTDIR}/all_annotations_both_strand_with_names.bed 
done
