#!/bin/sh

## 08_23_17_lncRNA_partition_generation.sh
## alex amlie-wolf
## the steps to create reference files for SPAR of hg19 and hg38 reference genomes
## these reference files only relate to lncRNA
## this is all run on scisub

## ---------------------------------------------------------
for REF in hg19 hg38; do
    echo "Parsing reference ${REF}"

    ## create the split bed files
    GFF_FILE=/project/wang/pkuksa/datasets/DASHRv2/annot/${REF}/${REF}_annot.lncRNA.unique_LOC.gff
    OUTDIR=/home/alexaml/data/refgenomes/${REF}/lncRNA_${REF}_SPAR_partition/

    mkdir -p ${OUTDIR}/bed_files/ 
    
    awk -F $'\t' -v OUT=${OUTDIR}/bed_files/ '{OUTF=OUT"/"$3".bed"; printf "%s\t%d\t%d\t%s\t0\t%s\n", $1, ($4-1), $5, $9, $7 > OUTF}' ${GFF_FILE}
    
    mkdir -p ${OUTDIR}/parsed_files/

    cd ${OUTDIR}/bed_files/
    
    echo "Parsing bed files for canonical chromosomes"
    for F in *; do
    	echo $F    
    	~/code/bed_statistics/parse_bedfile_chrs.sh $F ${OUTDIR}/parsed_files/parsed_${F}
    done

    mkdir -p ${OUTDIR}/strand_split_files/
    cd ${OUTDIR}/parsed_files/

    echo "Splitting files by strand"
    for F in *; do
    	echo $F
    	CLASS=`echo $F | cut -d. -f1`
    	awk '{if ($6=="+") print $0}' $F > ${OUTDIR}/strand_split_files/${CLASS}.pos.bed
    	awk '{if ($6=="-") print $0}' $F > ${OUTDIR}/strand_split_files/${CLASS}.neg.bed
    done

    ## now merge these files together
    cd ${OUTDIR}/strand_split_files/
    mkdir -p ${OUTDIR}/merged_files/

    module load bedtools2
    
    echo "Merging files"
    ## merge all of them
    for F in *; do
    	echo $F
    	bedtools merge -i ${F} -c 4 -o distinct -delim ";" > ${OUTDIR}/merged_files/${F%.bed}.merged.bed
    done

    cd ${OUTDIR}/merged_files/
    echo "Sorting merged files"
    ## sort the merged files
    for F in *.bed; do
    	echo $F
    	sort -k1,1V -k2,2n $F > $F.sorted
    	mv $F.sorted $F
    done

    ## now create exclusive exons and introns   
    echo "Generating exclusive lncRNA exons and introns"
    ## we only need the complement of the exons
    bedtools complement -i ${OUTDIR}/merged_files/parsed_lncRNA_exon.pos.merged.bed -g \
    	~/data/refgenomes/${REF}/${REF}.chrom.sizes.sorted > \
	${OUTDIR}/pos_non_lncRNA_exons.bed
    bedtools complement -i ${OUTDIR}/merged_files/parsed_lncRNA_exon.neg.merged.bed -g \
    	~/data/refgenomes/${REF}/${REF}.chrom.sizes.sorted > \
	${OUTDIR}/neg_non_lncRNA_exons.bed

    ## intersect the introns with the complement of the exons
    bedtools intersect -a ${OUTDIR}/merged_files/parsed_lncRNA_intron.pos.merged.bed -b ${OUTDIR}/pos_non_lncRNA_exons.bed > ${OUTDIR}/pos_ne_lncRNA_introns.bed
    bedtools intersect -a ${OUTDIR}/merged_files/parsed_lncRNA_intron.neg.merged.bed -b ${OUTDIR}/neg_non_lncRNA_exons.bed > ${OUTDIR}/neg_ne_lncRNA_introns.bed
    
    ## now merge the exon and intron files by strand
    >${OUTDIR}/pos_all_lncRNA_annotations.bed
    awk -F$'\t' 'BEGIN{OFS=FS} {$4="lncRNA_exon;"$4; print}' \
	${OUTDIR}/merged_files/parsed_lncRNA_exon.pos.merged.bed >> ${OUTDIR}/pos_all_lncRNA_annotations.bed
    awk -F$'\t' 'BEGIN{OFS=FS} {$4="lncRNA_intron;"$4; print}' \
	${OUTDIR}/pos_ne_lncRNA_introns.bed >> ${OUTDIR}/pos_all_lncRNA_annotations.bed    
    >${OUTDIR}/neg_all_lncRNA_annotations.bed
    awk -F$'\t' 'BEGIN{OFS=FS} {$4="lncRNA_exon;"$4; print}' \
	${OUTDIR}/merged_files/parsed_lncRNA_exon.neg.merged.bed >> ${OUTDIR}/neg_all_lncRNA_annotations.bed
    awk -F$'\t' 'BEGIN{OFS=FS} {$4="lncRNA_intron;"$4; print}' \
	${OUTDIR}/neg_ne_lncRNA_introns.bed >> ${OUTDIR}/neg_all_lncRNA_annotations.bed
    
    ## finally, merge the strand files together
    echo "Merging strand files into final master file!"
    awk -F$'\t' 'BEGIN{OFS==FS} {printf $0"\t.\t-\n"}' ${OUTDIR}/neg_all_lncRNA_annotations.bed > ${OUTDIR}/all_annotations_both_strand_with_names.bed
    awk -F$'\t' 'BEGIN{OFS==FS} {printf $0"\t.\t+\n"}' ${OUTDIR}/pos_all_lncRNA_annotations.bed >> ${OUTDIR}/all_annotations_both_strand_with_names.bed

    sort -k1,1 -k2,2n ${OUTDIR}/all_annotations_both_strand_with_names.bed > ${OUTDIR}/all_annotations.sorted.bed
    mv ${OUTDIR}/all_annotations.sorted.bed ${OUTDIR}/all_lncRNA_annotations_both_strand_with_names.bed 
done
